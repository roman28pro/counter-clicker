package ru.kra.counterkliker;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Integer click = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clicker(View view) {
        click++;
        TextView textView = (TextView)findViewById(R.id.txt);
        textView.setText(click.toString());
    }

    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        outState.putInt("click", click);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        click = savedInstanceState.getInt("click");
    }
}